from datetime import datetime, timedelta
from airflow import DAG
from airflow_dbt_python.operators.dbt import DbtRunOperator
from airflow.operators.bash import BashOperator 

with DAG(
    'unique_stores_sample_model',
    default_args={
        'depends_on_past': False,
        'email': ['caiolauro@gmail.com'],
        'email_on_failure': False,
        'email_on_retry': False,
        'retries': 1,
        'retry_delay': timedelta(minutes=5)
    },
    description='Runs Unique Stores Model',
    schedule_interval= timedelta(minutes=3),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['sample-dag'],
) as dag:

    run_dbt_agg_models = DbtRunOperator(
        task_id="update-unique-stores",
        project_dir="s3://airflow-dbt-bucket/transformations",
        profiles_dir="s3://airflow-dbt-bucket/dbt_profiles",
        # select=["+tag:daily"],
        # exclude=["tag:deprecated"],
        target="dev",
        profile="dbt_demo",
   )
    success = BashOperator(
        task_id='print_success',
        bash_command='echo "Success"'
    )

    run_dbt_agg_models >> success
